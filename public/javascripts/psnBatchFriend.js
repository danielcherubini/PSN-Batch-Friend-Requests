
/*
Hey! You're looking at the PSN batch friend javascript!
The fun stuff is not here, it's in the Node stuff. This just checks
to make sure users don't enter bad data.
- DrasticActions (@drasticactionSA)
*/
var pageSize = '';
var totalResults = '';

var getFriendsList = function(username, offset, friendStatus, index) {
	var json = {offset: $('#nextButton').data('page'), friendStatus: "friend", index: index};
	$.post( '/list', json, function(data) {
		var message = data.message;
		if(data.error)
		{
			message += ": " + data.response.error.message;
		}
		pageSize = data.response.size;
		totalResults = data.response.totalResults;
		if (pageSize >= totalResults) {
			console.log('yahoo')
		};
		$('#nextButton').data('page', pageSize+json.offset)
		$('#loadingConsole').append(new Option(message, ""));
		$('#progressBar').attr('value',index + 1);
		for (var i = 0; i < data.response.size; i++) {
			var obj = data.response.friendList[i];
			var html = '<div class="checkbox"><label><input type="checkbox" data-name='+obj.onlineId+'>'+obj.onlineId+'</input></label></div>'
			$('#friendsList').append(html);
		};
	});
}

var getNextPage = function() {
	var checkedBoxes = $('input[type="checkbox"]:checked');
	var uncheckedBoxes = $('input:checkbox:not(:checked)');
	$.each(checkedBoxes, function(index, item) {
		$('#whiteList').append($(item).data('name')+',');
	})
	$.each(uncheckedBoxes, function(index, item) {
		$('#blackList').append($(item).data('name')+',');
	})
	$('#friendsList').empty();
	getFriendsList($('#nextButton').data('page'), 'friend');
}

var removeFriends = function() {
	var blackListArray = $('#blackList').val().split(',');
	$('#progressBar').attr('max',blackListArray.length);
	for (var i = 0; i < blackListArray.length; i++) {
		json = {id: blackListArray[i], addFriend: false};
		doRemoveFriends(json, i);
		$('#progressBar').attr('value',i + 1);
	};
}

var doRemoveFriends = function(json, index) {
	console.log()
	$.post( '/remove', json, function(data) {
		var message = data.message;
		if(data.error)
		{
			message += ": " + data.response.error.message;
		}
		$('#loadingConsole').append(new Option(message, ""));
		$('#progressBar').attr('value',index + 1);
	});
}
var checkUsernames = function ()
{
	// Empty all of the user names from the parsed user name box.
	$('#parsedUsernames').empty();

	// Get all the user names from the text area.
	var lines = $('textarea[id=usernameTextarea]').val().split('\n');

	// Set up a regex to filter out unsupported usernames.
	// The PSN Online ID allows for alphanumeric characters, underscores and dashes.
	var re = new RegExp('^[-a-zA-Z0-9_]*$');

	// Reset the progress meter.
	$('#progressBar').attr('value', 0);
	$('#progressBar').attr('max',lines.length);
	$.each(lines, function(){
		// Get the current usernames in the listbox
		if(this.length >= 3 && this.length <= 16 && this.match(re))
		{
			$('#parsedUsernames').append(new Option(this, this));
		}
	});
}

var sendUsernames = function()
{
	var usernameArray = getUsernames();
	var re = new RegExp('^[-a-zA-Z0-9_]*$');
	var requestMessage = $('#blackList').val();
	for(var i = 0; i < usernameArray.length; i++)
	{
		if(usernameArray[i].length >= 3 && usernameArray[i].length <= 16 && usernameArray[i].match(re))
		{
			sendUsername(usernameArray[i], requestMessage, i);
		}
		else
		{
			var message = "Did not add - " + usernameArray[i];
			$('#loadingConsole').append(new Option(message, ""));
			$('#progressBar').attr('value',i + 1);
		}
	}
}


var sendUsername = function(username, requestMessage, index)
{
	var json = {id: username, requestMessage: requestMessage};
		$.post( '/psn', json, function(data) {
			var message = data.message;
			if(data.error)
			{
				message += ": " + data.response.error.message;
			}
			$('#loadingConsole').append(new Option(message, ""));
			$('#progressBar').attr('value',index + 1);
		});
}

var getUsernames = function()
{
	var lb = document.getElementById('parsedUsernames');
	var arr = [], opts = lb.options, len = opts.length;
	for (var i = 0; i < len; i++) {
    arr[i] = opts[i].value;
    }
    return arr;
}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
	res.render('remove', { title: 'A Batch App' });
});


router.post('/', function(req, res) {
	gumerPSN.addRemoveFriend(req.body.id, req.body.message, function(error, removeFriendData) {
			if (!error) {
				console.log('success!' + req.body.id);
				var json = { error: false, message: req.body.id + " - Successfully Removed!", response: removeFriendData };
				res.json(json);
			}
			else {
				console.log('failed! ' + req.body.id);
					res.json({
						error: true, message: req.body.id + " - Spectacularly failed!", response: removeFriendData
					});
			}
		})
});

module.exports = router;
